# Fuzzing for python Example

This is an example of how to integrate your [pythonfuzz](https://gitlab.com/gitlab-org/security-products/analyzers/fuzzers/pythonfuzz/-/tree/master) targets into GitLab CI/CD

This example will show the following steps:
* [Building and running a simple pythonfuzz target locally](#building-and-running-a-pythonfuzz-target)
* [Running the pythonfuzz target via GitLab CI/CD](#running-pythonfuzz-fuzz-from-ci)

Result:
* pythonfuzz targets will run a test on the master branch on every commit.
* pythonfuzz targets will run regression tests on every merge request (and every other branch) with the generated corpus and crashes to catch bugs early on.

Fuzzing for python can help find both complex bugs and correctness bugs. python is a safe language so memory corruption bugs
are unlikely to happen, but bugs can still have security implications like DoS attacks. See [Trophies](https://gitlab.com/gitlab-org/security-products/analyzers/fuzzers/pythonfuzz/-/tree/master#trophies)

This tutorial focuses less on how to build pythonfuzz targets and more on how to integrate the targets with GitLab. A lot of 
great information is available at the [pythonfuzz](https://gitlab.com/gitlab-org/security-products/analyzers/fuzzers/pythonfuzz) repository.

## Building and running a pythonfuzz target

The example is located in the [pythonfuzz](https://gitlab.com/gitlab-org/security-products/analyzers/fuzzers/pythonfuzz) repository.

## Running pythonfuzz from CI

The best way to integrate pythonfuzz fuzzing with Gitlab CI/CD is by adding additional stage & step to your `.gitlab-ci.yml`.

```yaml

image: python:latest

stages:
  - test
  - fuzz

unit_test:
  stage: test
  script:
    - echo "Stub for regular unit-tests"

include:
  - template: Coverage-Fuzzing.gitlab-ci.yml

my_fuzz_target:
  extends: .fuzz_base
  variables:
    FUZZ_EXAMPLE: "fuzzit"
  script:
    - pip install --extra-index-url https://gitlab.com/api/v4/projects/19904939/packages/pypi/simple pythonfuzz
    - ./gitlab-cov-fuzz run --engine pythonfuzz -- examples/${FUZZ_EXAMPLEg}/fuzz.py


```

For each fuzz target you will will have to create a step which extends `.fuzz_base` that runs the following:
* Builds the fuzz target
* Runs the fuzz target via `gitlav-cov-fuzz` CLI.
* Choose the `pythonfuzz` fuzz engine with `--engine pythonfuzz`
* For `$CI_DEFAULT_BRANCH` (can be override by `$COV_FUZZING_BRANCH`) will run fully fledged fuzzing sessions.
  For everything else including MRs will run fuzzing regression with the accumlated corpus and fixed crashes.  

